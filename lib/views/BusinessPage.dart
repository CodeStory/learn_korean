import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:korean_app/model/Words.dart';
/*
class BusinessPage extends StatefulWidget {
  static const routeName = "/Business-page";

  @override
  State<StatefulWidget> createState() {
    return _BusinessPageState();
  }
}
class _BusinessPageState extends State<BusinessPage>{
  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: AppBar(
          title:Text("Business Page"),
        ),
        body:SizedBox(
          child: BusinessWords(),
        )
    );
  }
}

class BusinessWords extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _BusinessWordsData();
  }
}
class _BusinessWordsData extends State<BusinessWords> {
  final int Korean = 1;
  final int KoreanRead = 2;
  final int soundPath = 3;
  String encoded;

  List<Words> data = List<Words>();

  _BusinessWordsData() {
    Words firstWord = Words(
        "안녕하세요", "an nyung ha se yo", "How do you do?", new File("path.txt"));
    Words secondWord = Words(
        "반갑습니다", "an nyung ha se yo", "Nice to meet you",File("path of sound"));

    data.add(firstWord);
    data.add(secondWord);

    data.add(firstWord);
    data.add(secondWord);

    data.add(firstWord);
    data.add(secondWord);
  }

  @override
  Widget build(BuildContext context) {
    TextStyle defaultStyle = new TextStyle(
      inherit: false,
      color: Colors.blue[600],
      fontSize: 18.0,
      fontStyle: FontStyle.normal,
      height: 2.0,
    );

    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: this.data.length,
        itemBuilder: (BuildContext context, int index) =>
            buildWordCard(context, index)
    );
  }

  Widget buildWordCard(BuildContext context, int index) {
    final currentData = data[index];
    return Container(
      child: Card(
        child:Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: new EdgeInsets.all(16.0),
              alignment: Alignment.centerLeft,
              child:new Column (
                children:<Widget>[
                  Text(
                    currentData.getKoreanWords(),
                  ),
                  Text(
                    currentData.getPronoun(),
                  ),
                ],
              ),
            ),

            /*Align(
                  alignment: Alignment(1,1),
                  child: new Icon(Icons.play_arrow),
                ),

                 */
            Container(
                margin: new EdgeInsets.only(right: 16.0),
                alignment: Alignment.centerRight,
                child:new IconButton(
                  icon: Icon(Icons.play_arrow),
                  onPressed: () =>{print("Hello" +  index.toString())},
                )
            ),
          ],
        ),
      ),
    );
  }
}
*/

import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:korean_app/model/Words.dart';
import 'package:korean_app/view_model/WordViewModel.dart';
import 'package:provider/provider.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:korean_app/views/widgets/WordListView.dart';

class BusinessPage extends StatefulWidget{
  static const routeName = "/Business-page";
  dynamic mWordData;

  BusinessPage(dynamic data){mWordData= data;}

  @override
  State<StatefulWidget> createState() {
    return _BusinessPage(mWordData);
  }
}

class _BusinessPage extends State<BusinessPage>{
  dynamic mWordData;
  _BusinessPage(dynamic data){this.mWordData = data;}

  @override
  Widget build(BuildContext context) {
    return DefaultTabController (
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Business Page"),
          bottom: TabBar(
              tabs:[
                Tab(icon: Icon(Icons.work)),
                Tab(icon:Icon(Icons.pregnant_woman))
              ]
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            new WordListView(mWordData.maleWords, "Business Page"),
            new WordListView(mWordData.femaleWords, "Business Page")
          ],
        ),
      ),
    );
  }
}


//Old version
/*
class BusinessPage extends StatefulWidget {
  static const routeName = "/Business-page";
  dynamic data;
  BusinessPage(dynamic words){data = words;}

  @override
  State<StatefulWidget> createState() {
    return _BusinessPageState(data);
  }
}
class _BusinessPageState extends State<BusinessPage>{
  final int Korean = 1;
  final int KoreanRead = 2;
  final int soundPath = 3;
  String encoded;
  List<dynamic> data;
  bool isDoubleTapped = false;
  String koreanWord = "Data Not Available";
  String pronounce = "Data is not available";

  _BusinessPageState(dynamic data){
    this.data = data.maleWords;
  }
  @override
  Widget build(BuildContext context){
    TextStyle defaultStyle = new TextStyle(
      inherit: false,
      color: Colors.blue[600],
      fontSize: 18.0,
      fontStyle: FontStyle.normal,
      height: 2.0,
    );

    return Scaffold(
        appBar: AppBar(
          title:Text("Business Page"),
        ),
        body:SizedBox(
            child: Center(
                child:ListView.builder(
                    padding: const EdgeInsets.all(8),
                    itemCount: this.data.length,
                    itemBuilder: (BuildContext context, int index) =>
                        buildWordCard(context, index)
                ),
            ),
        ),
    );
  }

  Widget buildWordCard(BuildContext context, int index) {
    final currentData = data[index];
    koreanWord = currentData.koreanWord;
    pronounce = currentData.pronoun;
    return Container(
      child: InkWell(
        highlightColor: Colors.blue[200],
        child: Card(
          child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                  margin: new EdgeInsets.all(16.0),
                  alignment: Alignment.centerLeft,
                  child:InkWell(
                    onTap: (){
                      setState(){

                      }
                    },
                    child: new Column (
                      children:<Widget>[
                        (currentData.isSelected)? Text("${currentData.engDef}"):Text('$koreanWord'),
                        (currentData.isSelected)? Text(""):Text('$pronounce'),
                      ],
                    ),
                  )
              ),
              Container(
                  margin: new EdgeInsets.only(right: 16.0),
                  alignment: Alignment.centerRight,
                  child:Row(
                    children: <Widget>[
                      //Icon indicator whether displayed vocab is Korean Or English.
                      new IconButton(
                        icon: Icon(
                            (currentData.isSelected? Icons.work:Icons.ac_unit)
                        ),
                        onPressed: () =>{
                          setState((){
                            (currentData.iconPlayButton == Icons.play_arrow)? currentData.setIconPlayButton(Icons.play_arrow): currentData.setIconPlayButton(Icons.pause_circle_outline);
                            //data[index] = currentData;
                            if(currentData.iconPlayButton == Icons.play_arrow)
                              currentData.setIconPlayButton(Icons.pause_circle_outline);
                            else currentData.setIconPlayButton(Icons.play_arrow);
                            print("Hello man " +  index.toString());
                          }),
                        },
                      ),
                      new IconButton(
                        icon: Icon(currentData.iconPlayButton),
                        onPressed: () =>{
                          setState((){
                            (currentData.iconPlayButton == Icons.play_arrow)? currentData.setIconPlayButton(Icons.play_arrow): currentData.setIconPlayButton(Icons.pause_circle_outline);
                            //data[index] = currentData;
                            if(currentData.iconPlayButton == Icons.play_arrow)
                              currentData.setIconPlayButton(Icons.pause_circle_outline);
                            else currentData.setIconPlayButton(Icons.play_arrow);
                            print("Hello man " +  index.toString());
                          }),
                        },
                      ),
                    ],
                  )
              ),
            ],
          ),
        ),
        onDoubleTap: ()=>{
          _onSelected(index)
        },
      ),
    );
  }
  _onSelected(index){
    setState(()=>{
      data[index].isSelected= !data[index].isSelected,
      print(data[index].isSelected),
      isDoubleTapped = !isDoubleTapped,
    });
    print("Double Taps");
  }

  LoveWords getData(WordViewModel vm){
    return vm.getLoveWordData();
  }
}

 */