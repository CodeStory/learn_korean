import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:korean_app/view_model/helper/AdsController.dart';
import 'package:korean_app/view_model/helper/Setting.dart';
import 'package:launch_review/launch_review.dart';

class MyBanner extends StatelessWidget{

  @override
  Widget build(BuildContext context) {

    Orientation orientation = MediaQuery.of(context).orientation;
    return Material(
      type: MaterialType.transparency,
      child: SizedBox(
        height:50.0,
        child: GestureDetector(
            child: Container(
                decoration: BoxDecoration(
                  color: Colors.blueGrey[600],
                ),
                child: Container(
                  padding: new EdgeInsets.fromLTRB(25.0, 2.0, 0.0,2.0),
                  child:Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Image.asset(
                        "assets/images/srt_player.png",
                        fit:BoxFit.fitHeight,
                      ),
                      Text("   "),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                              "SRT Media Player: Try Out!",
                              style: TextStyle(
                                fontSize: (orientation == Orientation.portrait)?15:25,
                                color: Colors.white,
                              )
                          ),
                        ],
                      ),
                    ],
                  ),
                )
            ),
            onTap:(){
//        LaunchReview.launch(androidAppId: , iOSAppId: );
            }
        ),
      )
    );
  }


}