import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:korean_app/model/CategoryData.dart';

import '../LovePage.dart';

class HomePageListView extends StatefulWidget{
  List<dynamic> mCategoryData;
  HomePageListView(List<dynamic> categoryData){
    mCategoryData = categoryData;
  }

  @override
  State<StatefulWidget> createState() {
    return HomePageListState(mCategoryData);
  }
}

class HomePageListState extends State<HomePageListView>{
  List<dynamic> mCategoryData;

  HomePageListState(List<dynamic> categoryData){
    mCategoryData = categoryData;
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(8),
      itemCount: this.mCategoryData.length,
      itemBuilder: (BuildContext context, int index) => buildHomePageCard(context,index)
    );
  }
  Widget buildHomePageCard(BuildContext context, int index){
    return new Card(
      shape:RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
      child:Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text("Testing"),
            Text("Testing")
   /*
            Column(
              children: <Widget>[
                InkWell(
                  child: Image.asset(
                    this.mImageAsset,
                    height: 150,
                    fit: BoxFit.fill,
                  ),
                  onTap: () {
                    String tempString = mImageAsset.toString().toLowerCase();
                    String endString;
                    if(tempString.contains("love")){
                      endString= "love";
                    }else if(tempString.contains("business")){
                      endString = "business";
                    }else{
                      endString ="travel";
                    }

                    print(endString);

                    switch(endString) {
                      case "love":
                        print(mTitle);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>
                          new LovePage(categories.loveWords)),
                        );
                        break;
                      case "business":
                        print(mTitle);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>
                          new BusinessPage(categories.businessWords)),
                        );
                        break;
                      case "travel":
                        print(mTitle);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>
                          new LovePage (categories.loveWords)),
                        );
                        break;
                      default:
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>
                          new LovePage(categories.loveWords)),
                        );
                    }
                  },
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Text(this.mTitle),
                InkWell(
                  //highlightColor: Colors.lightBlue[500],
                  splashColor: Colors.lightBlue[200],
                  onTap: (){},
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Expanded(
                        flex: 2,
                        child: Container(
                          alignment: Alignment.centerRight,
                          child:Text("Let\'s go!"),
                        ),
                      ),
                      new Flexible(
                        flex:2,
                        child: IconButton(
                            icon:Icon(Icons.arrow_forward_ios),
                            color:Colors.lightBlue[400],
                            onPressed: () {
                              switch(mTitle){
                                case "Love":
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder:(context)=> new LovePage(categories.loveWords)),
                                  );
                                  break;
                                case "Business":
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder:(context)=> new BusinessPage(categories.businessWords)),
                                  );
                                  break;
                                case "Travel":
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder:(context)=> new TravelPage(categories.travelWords)),
                                  );
                                  break;
                                default:
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder:(context)=> new LovePage(WordData.loveWords)),
                                  );
                              }
                              //Navigator.of(context).pushNamed(LovePage.routeName);
                            }
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
    */
          ]
      ),
    );
  }
}

/*

   */
