
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'file:///C:/Users/leeds_000/Desktop/Udacity/korean_app/lib/view_model/helper/Setting.dart';
import 'package:korean_app/view_model/MyAudioPlayer.dart';
import 'package:korean_app/view_model/WordViewModel.dart';
import 'package:korean_app/view_model/helper/AdsController.dart';
import 'package:provider/provider.dart';

class WordListView extends StatefulWidget {
  //static const routeName = "/Business-page";
  List<dynamic> data;
  String mTitle;

  WordListView(List<dynamic> wordData, String title){
    data = wordData;
    mTitle = title;
  }

  @override
  State<StatefulWidget> createState() {
    return _WordListViewState(this.data, mTitle);
  }
}
class _WordListViewState extends State<WordListView>{
  final int Korean = 1;
  final int KoreanRead = 2;
  final int soundPath = 3;
  String encoded;
  String mTitle;
  List<dynamic> data;
  int totalLength;
  bool isDoubleTapped = false;
  String koreanWord = "Data Not Available";
  String pronounce = "Data is not available";
  Setting setting;

  _WordListViewState(List<dynamic> data, String title){
    this.data = data;
    mTitle = title;
  }
  @override
  Widget build(BuildContext context){
    TextStyle defaultStyle = new TextStyle(
      inherit: false,
      color: Colors.blue[600],
      fontSize: 18.0,
      fontStyle: FontStyle.normal,
      height: 2.0,
    );
    totalLength =  this.data.length;
    return
      ChangeNotifierProvider(
        create: (BuildContext context) {return WordViewModel(); },
        child:ListView.builder(
            padding: const EdgeInsets.all(8),
            itemCount: totalLength+1,
            itemBuilder: (BuildContext context, int index) =>
                buildWordCard(context, index)
        )
      );

  }

  TextStyle responsiveFontSize(){
    return Setting.responsiveTextStyle();
  }

  TextStyle responsivePronSize(){
    return Setting.responsivePronSize();
  }

  Widget buildWordCard(BuildContext context, int index) {
    var currentData;

    if(index == totalLength)
      currentData = data[index-1];
    else
      currentData = data[index];

    koreanWord = currentData.koreanWord;
    pronounce = currentData.pronoun;

    setting = Setting(context);

    return  (index != totalLength)? InkWell(
      highlightColor: Colors.blue[200],
      child: Card(
          child:Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              //handle texts
              Expanded(
                flex:8,
                child:Container(
                    margin: new EdgeInsets.fromLTRB(16.0,8.0,0.8,8.0),
                    alignment: Alignment.centerLeft,
                    child:InkWell(
                      onTap: (){
                        /*play sound when text file is touched*/
                        print(index);
                        _playButton(index, currentData);
                      },
                      child: new Column (
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children:<Widget>[
                          (currentData.isSelected)? Text('$koreanWord', style: responsiveFontSize()):Text("${currentData.engDef}", maxLines: 2, style: responsiveFontSize()),
                          Text(""),
                          (currentData.isSelected)? Text('$pronounce', style: responsivePronSize()):Text(""),
                        ],
                      ),
                    )
                ),
              ),
              Flexible(
                flex:2,
                fit:FlexFit.tight,
                child:FittedBox(
                    //margin: new EdgeInsets.only(right: 16.0),
                    alignment: Alignment.center,
                    child:Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        //Icon indicator whether displayed vocab is Korean Or English.
                        new IconButton(
                          icon: new Image.asset((currentData.isSelected? "assets/images/E.png": "assets/images/K.png"), width: 18,),
                          onPressed: () =>{
                            _onSelected(currentData)
                          },
                        ),  // Button that controls play - disabled
                        /*new IconButton(
                          icon: Icon(currentData.isPlaying? Icons.pause:Icons.play_arrow),

                          onPressed: () =>{
                            //Play Sound base on the index/item it clicked
                            _playButton(vm, index)
                          },


                        ),

                       */
                      ],
                    )
                ),
              )
            ],
          )
      ),
      onDoubleTap: ()=>{
        _onSelected(currentData)
      },
      onTap:()=>{
        _playButton(index, currentData)
      },
    ):AdsController.load_FB_NativeAd();
  }
  _onSelected(currentData){
    setState(()=>{
      currentData.isSelected= !currentData.isSelected,
      print(currentData.isSelected),
      isDoubleTapped = !isDoubleTapped,
    });
    print("Double Taps");
  }

  _playButton(int index, currentData) async{
    MyAudioPlayer.playButton(index, currentData);
  }

/*
  AudioPlayer ap = AudioPlayer();
  bool inUse = false;
  _playButton(vm, int index) async{

    setState(() {
      data[index].isPlaying = true;
    });

    if(inUse){
      _inUse(data[index]);
    }else{
      _notInUse(data[index]);
    }
  }

  _inUse(dynamic selectedData){
    _stopMedia();
    if(selectedData.isPlaying) {
      setState(() {
        inUse = true;
      });
      AudioCache audioCache = AudioCache(prefix: 'sounds/', fixedPlayer: ap);
      //audioCache.play('five-2.mp3');
      print(selectedData.soundPath);
      audioCache.play(selectedData.soundPath);

      ap.onPlayerCompletion.listen((event) {
        ap.release();
        audioCache.clearCache();
        setState(() {
          selectedData.isPlaying = false;
          inUse = false;
        });
      });
    }
  }
  _notInUse(dynamic selectedData){
    if(selectedData.isPlaying){
      setState(() {
        inUse = true;
      });
      AudioCache audioCache = AudioCache(prefix:'sounds/', fixedPlayer: ap);
      //audioCache.play('five.mp3');
      audioCache.play(selectedData.soundPath);

      ap.onPlayerCompletion.listen((event) {
        ap.release();
        audioCache.clearCache();
        setState(() {
          selectedData.isPlaying = false;
          inUse = false;
        });
      });
    }
  }
  _stopMedia(){
    ap.stop();
    ap.release();
  }
*/
}