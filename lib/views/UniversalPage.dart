import 'package:facebook_audience_network/ad/ad_banner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:korean_app/view_model/MyAudioPlayer.dart';
import 'package:korean_app/view_model/helper/Setting.dart';
import 'package:korean_app/views/HomePage.dart';
import 'package:korean_app/views/widgets/MyBanner.dart';
import 'package:korean_app/views/widgets/WordListView.dart';
import '../view_model/helper/AdsController.dart';

class UniversalPage extends StatefulWidget {
  dynamic mWordData;

  UniversalPage(dynamic data) {
    mWordData = data;
  }

  @override
  State<StatefulWidget> createState() {
    return _UniversalPageState(mWordData);
  }
}

class _UniversalPageState extends State<UniversalPage> {
  dynamic data;
  Widget banner;
  static const  APPBAR_SIZE = 50.0;
  static const NUMTABS=2;

  _UniversalPageState(dynamic data) {
    this.data = data;
  }
 void loadBanner() {
    //return AdsController.loadBanner();
   setState(() {
     banner= AdsController.loadBanner();
   });
  }

  @override
  Widget build(BuildContext context) {
    banner = AdsController.substituteBanner();
    loadBanner();
    _hideStatusBar();

    return Material(
      child: Scaffold(
        body:DefaultTabController(
            length: NUMTABS,
            child: new WillPopScope(
                child: CustomScrollView(
                  //physics: const BouncingScrollPhysics(),
                  physics: const ClampingScrollPhysics(),
                  slivers: <Widget>[
                    //Advertisement
                    SliverToBoxAdapter(
                      child:AdsController.load_FB_NativeBanner(),// banner,
                    ),
                    //Appbar
                    SliverAppBar(
                      automaticallyImplyLeading: true,
                      primary: false, //indicate whether appbar is being displayed at top or not: it removed top padding.
                      expandedHeight: 50.0,
                      backgroundColor: Colors.blueGrey[600],
                      iconTheme: IconThemeData(
                        size:35.0,
                        color:Colors.white,
                      ),
                      title: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "${data.title} Page",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: APPBAR_SIZE/2,
                              fontStyle: FontStyle.italic,//Colors.amber
                            ),
                          ),
                        ],
                      ),
                      pinned: true,
                      floating: true,
                      snap: true,
                      stretch: true,
                      stretchTriggerOffset: 20.0,
                    ),
                    //Tabs
                    SliverToBoxAdapter(
                      child:TabBar(
                          labelColor: Colors.white,
                          indicatorColor: Colors.amber,
                          //unselectedLabelColor: Colors.amber,
                          tabs:[
                            Tab(icon: Icon(
                              Icons.face,
                              size: 32.0,
                              color:Colors.cyan[800],
                            )),
                            Tab(icon:Icon(
                              Icons.face,
                              size:32.0,
                              color:Colors.red[400],
                            ))
                          ]
                      ),
                    ),
                    //AppBody - listviews
                    SliverFillRemaining(
                      child:TabBarView(
                        children: <Widget>[
                          new WordListView(data.maleWords, "${data.title} Page"),
                          new WordListView(data.femaleWords, "${data.title} Page")
                        ],
                      ),
                    )
                  ],
                ),
                /*
          child: Scaffold(
          /*
            appBar: AppBar(
            backgroundColor: Colors.blueGrey[600],
            title: Text("${data.title} Page"),
            bottom: TabBar(
                labelColor: Colors.white,
                indicatorColor: Colors.amber,
                //unselectedLabelColor: Colors.amber,
                tabs:[
                  Tab(icon: Icon(
                    Icons.face,
                    size: 32.0,
                    color:Colors.black,
                  )),
                  Tab(icon:Icon(
                      Icons.face,
                      size:32.0,
                      color:Colors.red[100]
                  ))
                ]
            ),
          ),
          */
          /*
              CustomScrollView(
                physics: const BouncingScrollPhysics(),
                slivers: <Widget>[
                  SliverToBoxAdapter(
                    child: SafeArea(
                        child: Text("Testing"),
                    )
                  ),

                  SliverFillRemaining(
                    child:TabBarView(
                      children: <Widget>[
                        new WordListView(data.maleWords, "${data.title} Page"),
                        new WordListView(data.femaleWords, "${data.title} Page")
                      ],
                    ),
                  )
                ],
              )
            */
          body:CustomScrollView(
            physics: const BouncingScrollPhysics(),
            slivers: <Widget>[
              //Advertisement
              SliverToBoxAdapter(
                  child: loadBanner()
                  /*
                  child: SafeArea(
                    child: loadBanner()
                    //child: Text("Advertisement"),
                  )

                   */
              ),
              //Appbar
              SliverToBoxAdapter(
                child: PreferredSize(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.blueGrey[600],
                      //title: Text("${data.title} Page"),
                    ),
                    child: Row(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.arrow_back,color: Colors.white,size: APPBAR_SIZE-20,),
                          onPressed: (){
                            _navigateToHome();
                          } ,
                        ),
                        Text(
                            "${data.title} Page",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: APPBAR_SIZE/2,
                            fontStyle: FontStyle.italic,//Colors.amber
                          ),
                        ),
                      ],
                    ),
                  ),
                  preferredSize: Size.fromHeight(APPBAR_SIZE),

                )
              ),
              //Tabs
              SliverToBoxAdapter(
                child:TabBar(
                        labelColor: Colors.white,
                        indicatorColor: Colors.amber,
                        //unselectedLabelColor: Colors.amber,
                        tabs:[
                          Tab(icon: Icon(
                            Icons.face,
                            size: 32.0,
                            color:Colors.cyan[800],
                          )),
                          Tab(icon:Icon(
                              Icons.face,
                              size:32.0,
                              color:Colors.red[400],
                          ))
                        ]
                    ),
              ),
              //AppBody - listviews
              SliverFillRemaining(
                child:TabBarView(
                  children: <Widget>[
                    new WordListView(data.maleWords, "${data.title} Page"),
                    new WordListView(data.femaleWords, "${data.title} Page")
                  ],
                ),
              )
            ],
          )
        ),
           */
                onWillPop: _onWillPop
            )
        ),
      )
    );

  }
  Future<bool> _onWillPop() {
    _navigateToHome();
  }
  _navigateToHome(){
    _stopMediaPlayer();
    //Navigate to home page without creating back button/trace.
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => new HomePage()),
            (route) => false);
  }
  _stopMediaPlayer(){
    MyAudioPlayer.stopMedia();
  }
  _hideStatusBar(){
    Setting.hideStatusBar();
  }
}
