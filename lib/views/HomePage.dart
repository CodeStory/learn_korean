import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:korean_app/model/CategoryData.dart';
import 'file:///C:/Users/leeds_000/Desktop/Udacity/korean_app/lib/view_model/helper/Setting.dart';
import 'package:korean_app/view_model/WordViewModel.dart';
import 'package:korean_app/view_model/helper/AdsController.dart';
import 'package:korean_app/views/widgets/MyBanner.dart';
import 'package:korean_app/views/UniversalPage.dart';
import 'package:loading_gifs/loading_gifs.dart';
import 'package:provider/provider.dart';

import 'LovePage.dart';
import 'package:advertising_id/advertising_id.dart';
import 'package:facebook_audience_network/facebook_audience_network.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {

    AdsController adsController = AdsController();
    getID();
    hideStatusBar();
    return _HomePageState();
  }

  Future getID() async {
    //Amazon f31d5405-7c73-45f3-9a4a-831b04918fb4
    print("Device ID is :${await AdvertisingId.id}");
  }

  hideStatusBar() {
    Setting.hideStatusBar();
  }
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    //AdsController adsController = AdsController();
    return MaterialApp(
      home:Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
        //Banner for starting page
          Flexible(
              flex: 1,
              child:  MyBanner(),
          ),
        //MVVM - Loading data for homepage
          Expanded(
            flex: 9,
            child: ChangeNotifierProvider(
              create: (BuildContext context) {
                return WordViewModel();
              },
              child: _HomePageStateless(),
            ),
          )
        ],
      ),
    );
  }
}

class _HomePageStateless extends StatelessWidget {
  WordViewModel wVM;
  @override
  Widget build(BuildContext context) {
    wVM = Provider.of<WordViewModel>(context);
    wVM.fetchWords();
    if(!WordViewModel.dataReady){
      print("showing loadingPage");
      //show loading page
      return _LoadingPage();
    }else return _HomePageStatelessTwo(wVM);
  }
}
class _LoadingPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    Setting setting = new Setting(context);
    return Material(
      type: MaterialType.transparency,
      child: Expanded(

        child:  Container(
            padding: EdgeInsets.all(16.0),
            decoration:BoxDecoration(
                color:Colors.white
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                FadeInImage.assetNetwork(placeholder:cupertinoActivityIndicator, image: "image.png"),
                Image.asset(circularProgressIndicator, scale:10),
                Text(
                  "Loading...",
                  style:Setting.responsiveTextStyle(),
                ),
                AdsController.load_FB_NativeBanner(),
              ],
            )
        ),
      )
    );
  }

}

class _HomePageStatelessTwo extends StatelessWidget {
  var loading = false;
  var newWords;
  bool isPortrait;
  WordViewModel wVM;

  _HomePageStatelessTwo(WordViewModel vm) {
    wVM = vm;
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);

    if (mediaQueryData.orientation == Orientation.landscape) {
      isPortrait = false;
    } else {
      isPortrait = true;
    }
    return portrait(isPortrait);
    //return (isPortrait)? portrait():landScape();
  }

  portrait(bool portrait) {
    /*
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                  children:[
                    Expanded(
                      child: Category(wVM.getLoveWordData()),
                    ),
                    Expanded(
                      child: Category(wVM.getBusinessWordsData()),
                    ),
                    Expanded(
                      child: Category(wVM.getTravelWordsData()),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                  children:[
                    Expanded(
                      child: Category(wVM.getLoveWordData()),
                    ),
                    Expanded(
                      child: Category(wVM.getBusinessWordsData()),
                    ),
                    Expanded(
                      child: Category(wVM.getTravelWordsData()),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                  children:[
                    Expanded(
                      child: Category(wVM.getLoveWordData()),
                    ),
                    Expanded(
                      child: Category(wVM.getBusinessWordsData()),
                    ),
                    Expanded(
                      child: Category(wVM.getTravelWordsData()),
                    ),
                  ],
                ),
     */
    bool isPortrait = portrait;

    return CustomScrollView(
      primary: false,
      slivers: <Widget>[
        SliverPadding(
          padding: (isPortrait)
              ? const EdgeInsets.only(top: 25)
              : const EdgeInsets.only(top: 0),
          sliver: SliverGrid.count(
            childAspectRatio: 3 / 3.8,
            crossAxisSpacing: 0,
            mainAxisSpacing: 10,
            crossAxisCount: 3,
            children: <Widget>[
              Category(wVM.getLoveWordData()),
              Category(wVM.getBusinessWordsData()),
              Category(wVM.getTravelWordsData()),
              Category(wVM.getSupermarketWords()),
              Category(wVM.getRestaurantWords()),
              Category(wVM.getShoppingWords()),
              Category(wVM.getFamilyWords()),
              Category(wVM.getFriendWords()),
              Category(wVM.getAnimalWords()),
              Category(wVM.getEmergencyWords()),
              Category(wVM.getHospitalWords()),
              Category(wVM.getRandomWords()),
            ],
          ),
        ),
        SliverToBoxAdapter(
          child: AdsController.load_FB_NativeBanner(),
        )
      ],
    );
  }
  landScape() {
    return SingleChildScrollView(
      child: ConstrainedBox(
        constraints: BoxConstraints(),
        child: Column(children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                child: Category(wVM.getLoveWordData()),
              ),
              Expanded(
                child: Category(wVM.getBusinessWordsData()),
              ),
              Expanded(
                child: Category(wVM.getTravelWordsData()),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                child: Category(wVM.getLoveWordData()),
              ),
              Expanded(
                child: Category(wVM.getBusinessWordsData()),
              ),
              Expanded(
                child: Category(wVM.getTravelWordsData()),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                child: Category(wVM.getLoveWordData()),
              ),
              Expanded(
                child: Category(wVM.getBusinessWordsData()),
              ),
              Expanded(
                child: Category(wVM.getTravelWordsData()),
              ),
            ],
          ),
        ]),
      ),
    );
  }
}

class Category extends StatelessWidget {
  String title;
  String imageAsset;
  List<CategoryData> cData;
  Setting setting;
  int _screenWidth;
  int _screenHeight;
  dynamic data;

  Category(dynamic data) {
    this.title = data.title;
    this.imageAsset = data.image;
    this.data = data;
  }

  _deviceScreenSize(width, height) {
    this._screenWidth = width.floor();
    this._screenHeight = height.floor();
  }

  double _responsiveSize() {
    // Galaxy S5 is Size(360.0, 640.0)- portrait && Size(640.0, 360.0) - landscape()
    // Galaxy S8 is size is Size(411.4, 830.9) - portrait &&  size is Size(830.9, 411.4) - landscape()
    if (this._screenHeight <= 360) {
      return 170.0;
    } else if (this._screenHeight < 450) {
      return 215.0;
    } else if (this._screenHeight < 650) {
      return 250.0;
    }
  }

  @override
  Widget build(BuildContext context) {
    //fetchWord
    //Provider.of<WordViewModel>(context).fetchWords();
    //final vm = Provider.of<WordViewModel>(context);
    setting = Setting(context);
    Setting.updateSetting();

    //final mediaQueryData = MediaQuery.of(context);
    //_deviceScreenSize(mediaQueryData.size.width, mediaQueryData.size.height);

    //print("size is ${mediaQueryData.size}");
    bool isPortrait;
    List<CategoryData> listOfCategory;
    if (Setting.getOrientation() == Orientation.landscape) {
      isPortrait = false;
    } else {
      isPortrait = true;
    }
/*
    if(vm.getWordData() != null){
      WordData = vm.getWordData();
    }
*/   //return _LoadingPage();
    return Container(
        margin: EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Expanded(
                flex: 10,
                child: MediaQuery.removePadding(
                    context: context,
                    removeBottom: true,
                    child: MediaQuery.removeViewInsets(
                      removeBottom: true,
                      context: context,
                      child: new Card(
                        margin: EdgeInsets.zero,
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.0))),
                        child: portrait(context),
                        //child:(isPortrait)? portrait(context):landScape(context),
                      ),
                    ))
                /*
          MediaQuery.removePadding(context: context, child: new Card(
            margin: EdgeInsets.zero,
            elevation:4,
            shape:RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20.0))),
            child: portrait(context),
            //child:(isPortrait)? portrait(context):landScape(context),
          ),)

           */
                )
          ],
        )

        /*

       */
        );
  }

  changePage(BuildContext context, String title) {
    switch (title) {
      default:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => new UniversalPage(data)),
          //              builder: (context) => new LovePage(WordData.loveWords)),
        );
    }
  }

  portrait(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(8.0)
          //topLeft: Radius.circular(8.0),
          //topRight: Radius.circular(8.0),
          ),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Flexible(
              flex: 8,
              child: Container(
                  color: Colors.red,
                  child: SizedBox.expand(
                    child: InkWell(
                      child: Image.asset(
                        this.imageAsset,
                        //height: 250,
                        //width: _responsiveSize(),
                        fit: BoxFit.fitHeight,
                      ),
                      onTap: () {
                        changePage(context, title);
                      },
                    ),
                  )),
            ),
            Flexible(
                flex: 2,
                child: InkWell(
                  //highlightColor: Colors.lightBlue[500],
                  splashColor: Colors.lightBlue[200],
                  onTap: () {
                    changePage(context, this.title);
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      InkWell(
                        //highlightColor: Colors.lightBlue[500],
                        splashColor: Colors.lightBlue[200],
                        onTap: () {
                          changePage(context, this.title);
                        },

                        child: Text(this.title,
                          style: Setting.responsiveTextStyle(),
                        ),
                      )
                    ],
                  ),
                ))
          ]),
    );
    /*
        new Padding(padding:new EdgeInsets.only(bottom:0.0), child:Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[

              //Image
              Flexible(
                flex: 7,
                child: InkWell(
                  child: Image.asset(
                    this.imageAsset,
                    height: 250,
                    width:_responsiveSize(),
                    fit: BoxFit.fill,
                  ),
                  onTap: () {
                    changePage(context, title);
                  },
                ),
              ),
              //Button
              Flexible(
                  flex:3,
                  child: InkWell(
                    //highlightColor: Colors.lightBlue[500],
                    splashColor: Colors.lightBlue[200],
                    onTap: (){
                      changePage(context, this.title);
                    },
                    child:

                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children:<Widget>[
                        InkWell(
                          //highlightColor: Colors.lightBlue[500],
                          splashColor: Colors.lightBlue[200],
                          onTap: (){
                            changePage(context, this.title);
                          },

                          child: Text(this.title),
                        )
                      ],
                    ),
                  )
              )
            ]
        ))
*/
  }

  landScape(BuildContext context) {
    return ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8.0),
          topRight: Radius.circular(8.0),
        ),
        child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Flexible(
                  flex: 8,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      InkWell(
                        child: Image.asset(
                          this.imageAsset,
                          height: Setting.responsiveImageSize(),
                          fit: BoxFit.fill,
                        ),
                        onTap: () {
                          changePage(context, this.title);
                          /*
                  String tempString = imageAsset.toString().toLowerCase();
                  String endString;
                  if(tempString.contains("love")){
                    endString= "love";
                  }else if(tempString.contains("business")){
                    endString = "business";
                  }else{
                    endString ="travel";
                  }

                  print(endString);

                  switch(endString) {
                    case "love":
                      print(title);
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) =>
                        new LovePage(data)),
                      );
                      break;
                    case "business":
                      print(title);
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) =>
                        new BusinessPage(data)),
                      );
                      break;
                    case "travel":
                      print(title);
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) =>
                        new TravelPage (data)),
                      );
                      break;
                    default:
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) =>
                        new LovePage(data)),
                      );
                  }

                   */
                        },
                      ),
                    ],
                  )),
              Expanded(
                  flex: 3,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(this.title,
                        style: Setting.responsiveTextStyle(),
                      ),
                      InkWell(
                        //highlightColor: Colors.lightBlue[500],
                        splashColor: Colors.lightBlue[200],
                        onTap: () {},
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Expanded(
                              flex: 2,
                              child: Container(
                                alignment: Alignment.center,
                                child: Text("Let\'s go!"),
                              ),
                            ),
                            new Flexible(
                              flex: 2,
                              child: Container(
                                alignment: Alignment.center,
                                child: IconButton(
                                    icon: Icon(Icons.arrow_forward_ios),
                                    color: Colors.lightBlue[400],
                                    onPressed: () {
                                      changePage(context, this.title);
/*
                            switch(title){
                              case "Love":
                                print(title);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder:(context)=> new LovePage(data)),
                                );
                                break;
                              case "Business":
                                print(title);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder:(context)=> new BusinessPage(data)),
                                );
                                break;
                              case "Travel":
                                print(title);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder:(context)=> new TravelPage(data)),
                                );
                                break;
                              default:
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder:(context)=> new LovePage(data)),
                                );
                            }*/
                                      //Navigator.of(context).pushNamed(LovePage.routeName);
                                    }),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ))
            ]));
  }
}
