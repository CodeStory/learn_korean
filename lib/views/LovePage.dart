import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:korean_app/views/widgets/WordListView.dart';

class LovePage extends StatefulWidget{
  static const routeName = "/Love-page";
  dynamic loveWordData;

  LovePage(dynamic data){loveWordData = data;}

  @override
  State<StatefulWidget> createState() {
    return _LovePageState(loveWordData);
  }
}

class _LovePageState extends State<LovePage>{
  dynamic data;
  _LovePageState(dynamic data){this.data = data;}

  @override
  Widget build(BuildContext context) {
    return DefaultTabController (
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Love Page"),
          bottom: TabBar(
              tabs:[
                Tab(icon: Icon(
                    Icons.face,
                    size: 32.0,
                    color:Colors.black,
                )),
                Tab(icon:Icon(
                    Icons.face,
                    size:32.0,
                    color:Colors.red[100]
                ))
              ]
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            new WordListView(data.maleWords, "Love Page"),
            new WordListView(data.femaleWords, "Love Page")
          ],
        ),
      ),
    );
  }
}