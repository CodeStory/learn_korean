import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:korean_app/model/Words.dart';
import 'package:korean_app/view_model/WordViewModel.dart';
import 'package:provider/provider.dart';

class TestingPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _TestingPageState();
  }

}

class _TestingPageState extends State<TestingPage>{

  @override
  void initState() {
    super.initState();
    Provider.of<WordViewModel>(context,listen: false).fetchWords();
  }

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<WordViewModel>(context);
    if(vm.getWordData().loveWords.maleWords[2].isSelected !=null){
      setState(() {
        vm.getWordData().loveWords.maleWords[2].isSelected = false;
      });
    }
//'${vm.dataWords[0].toString()}'
    return Text("Testing ${vm.getWordData().loveWords.maleWords[2].isSelected.toString()}");
  }

}