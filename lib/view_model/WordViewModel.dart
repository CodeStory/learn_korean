
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:korean_app/model/Services.dart';
import 'package:korean_app/model/Words.dart';

class WordViewModel extends ChangeNotifier{

  //List<MaleWords> dataWords = List<MaleWords>();
  Words _dataWords = Words();
  static bool dataReady = false;

  AudioPlayer audioPlayer = AudioPlayer();
  AudioCache audioCache = AudioCache();

  bool isPlaying = true;
  Map<int,bool> testing;

  Future<void> fetchWords() async{
    final result = await Services().fetchWords();

    // print(result);
    this._dataWords = result;
    //print(_dataWords.businessWords.toString());
    dataReady = true;
    notifyListeners();
  }

  Words getWordData(){
    return _dataWords;
  }

  LoveWords getLoveWordData(){
    return _dataWords.loveWords;
  }
  BusinessWords getBusinessWordsData(){
    return _dataWords.businessWords;
  }
  TravelWords getTravelWordsData(){
    return _dataWords.travelWords;
  }

  SupermarketWords getSupermarketWords(){
    return _dataWords.supermarketWords;
  }
  RestaurantWords getRestaurantWords(){
    return _dataWords.restaurantWords;
  }
  ShoppingWords getShoppingWords(){
    return _dataWords.shoppingWords;
  }

  FamilyWords getFamilyWords() {
    return _dataWords.familyWords;
  }
  FriendWords getFriendWords(){
    return _dataWords.friendWords;
  }
  AnimalWords getAnimalWords(){
    return _dataWords.animalWords;
  }

  EmergencyWords getEmergencyWords(){
    return _dataWords.emergencyWords;
  }
  HospitalWords getHospitalWords(){
    return _dataWords.hospitalWords;
  }
  RandomWords getRandomWords(){
    return _dataWords.randomWords;
  }


  Future<void> playAudioFile (int index) async {
    isPlaying = true;

    audioCache = AudioCache(prefix:'sounds/', fixedPlayer: audioPlayer);
    audioCache.play('five.mp3');
    audioPlayer.onPlayerCompletion.listen((event) {
      isPlaying = false;
      audioPlayer.release();
      audioCache.clearCache();
      return isPlaying;
    });
  }

}