import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Setting{
  static int screenWidth;
  static int screenHeight;
  static MediaQueryData mediaQueryData;

  static const int PRON_ADJUSTER = 4;
  static const double LANDSCAPE_ADJUSTER = 1.2;

  Setting(BuildContext context){
    mediaQueryData = MediaQuery.of(context);
  }

  static updateSetting(){
    //print("Updating...");
    screenWidth = mediaQueryData.size.width.floor();
    //print("screenWidth - ${screenWidth}");
    screenHeight = mediaQueryData.size.height.floor();
    //print("screenHeight- ${screenHeight}");
  }

  static double responsiveImageSize() {
    // Galaxy S5 is Size(360.0, 640.0)- portrait && Size(640.0, 360.0) - landscape()
    // Galaxy S8 is size is Size(411.4, 830.9) - portrait &&  size is Size(830.9, 411.4) - landscape()
    if (screenHeight <= 360) {
      return 170.0;
    } else if (screenHeight < 450) {
      return 215.0;
    } else if (screenHeight < 650) {
      return 250.0;
    }
  }
  static TextStyle responsiveTextStyle() {
    // Galaxy S5 is Size(360.0, 640.0)- portrait && Size(640.0, 360.0) - landscape()
    // Galaxy S8 is size is Size(411.4, 830.9) - portrait &&  size is Size(830.9, 411.4) - landscape()
// Fire8 is screenWidth - 600 & screenHeight- 913  - portrait && screenWidth - 961 & screenHeight- 552- landScape
    updateSetting();

    if(getOrientation() == Orientation.portrait){
      if (screenWidth <= 360) {
        return TextStyle(
            decoration: TextDecoration.none,
            fontSize: 12.0,
            color: Colors.black
        );
      } else if (screenWidth < 450) {
        return TextStyle(
            decoration: TextDecoration.none,
            fontSize: 15.0,
            color: Colors.black
        );
      } else if (screenWidth < 650) {
        return TextStyle(
          decoration: TextDecoration.none,
            fontSize: 25.0,
            color: Colors.black
        );
      }
    }

    if(getOrientation() == Orientation.landscape){
      if (screenHeight <= 360) {
        return TextStyle(
            decoration: TextDecoration.none,
            fontSize: 12.0*LANDSCAPE_ADJUSTER,
            color: Colors.black
        );
      } else if (screenHeight < 400) {
        return TextStyle(
            decoration: TextDecoration.none,
            fontSize: 15.0*LANDSCAPE_ADJUSTER,
            color: Colors.black
        );
      } else if (screenHeight < 650) {
        return TextStyle(
            decoration: TextDecoration.none,
            fontSize: 25.0*LANDSCAPE_ADJUSTER,
            color: Colors.black
        );
      }
    }
  }
  static double responsiveFontSize(Orientation orientation) {
    // Galaxy S5 is Size(360.0, 640.0)- portrait && Size(640.0, 360.0) - landscape()
    // Galaxy S8 is size is Size(411.4, 830.9) - portrait &&  size is Size(830.9, 411.4) - landscape()
// Fire8 is screenWidth - 600 & screenHeight- 913  - portrait && screenWidth - 961 & screenHeight- 552- landScape
    updateSetting();

    if(orientation == Orientation.portrait){
      if (screenWidth <= 360) {
        return  12.0;
      } else if (screenWidth < 450) {
        return  15.0;
      } else if (screenWidth < 650) {
        return  25.0;
      }
    }

    if(orientation == Orientation.landscape){
      if (screenHeight <= 360) {
        return  12.0*LANDSCAPE_ADJUSTER;
      } else if (screenHeight < 400) {
        return  15.0*LANDSCAPE_ADJUSTER;
      } else if (screenHeight < 650) {
        return  25.0;
      }
    }
  }
  static TextStyle responsivePronSize() {
    // Galaxy S5 is Size(360.0, 640.0)- portrait && Size(640.0, 360.0) - landscape()
    // Galaxy S8 is size is Size(411.4, 830.9) - portrait &&  size is Size(830.9, 411.4) - landscape()
// Fire8 is screenWidth - 600 & screenHeight- 913  - portrait && screenWidth - 961 & screenHeight- 552- landScape
    if(getOrientation() == Orientation.portrait){
      if (screenWidth <= 360) {
        return TextStyle(
            decoration: TextDecoration.none,
            fontStyle: FontStyle.italic,
            fontSize: 12.0 - PRON_ADJUSTER
        );
      } else if (screenWidth < 450) {
        return TextStyle(
            decoration: TextDecoration.none,
            fontStyle: FontStyle.italic,
            fontSize: 15.0 - PRON_ADJUSTER);
      } else if (screenWidth < 650) {
        return TextStyle(
            decoration: TextDecoration.none,
            fontStyle: FontStyle.italic,
            fontSize: 25.0 - PRON_ADJUSTER);
      }
    }

    if(getOrientation() == Orientation.landscape){
      if (screenHeight <= 360) {
        return TextStyle(
            decoration: TextDecoration.none,
            fontStyle: FontStyle.italic,
            fontSize: 12.0*LANDSCAPE_ADJUSTER - PRON_ADJUSTER);
      } else if (screenHeight < 400) {
        return TextStyle(
            decoration: TextDecoration.none,
            fontStyle: FontStyle.italic,
            fontSize: 15.0*LANDSCAPE_ADJUSTER - PRON_ADJUSTER);
      } else if (screenHeight < 650) {
        return TextStyle(
            decoration: TextDecoration.none,
            fontStyle: FontStyle.italic,
            fontSize: 25.0*LANDSCAPE_ADJUSTER - PRON_ADJUSTER);
      }
    }
  }
  static getOrientation(){
    //print( mediaQueryData.orientation);
    return mediaQueryData.orientation;
  }

  static hideStatusBar(){
    SystemChrome.setEnabledSystemUIOverlays([]);
  }
  static showStatusBar(){
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  }


}