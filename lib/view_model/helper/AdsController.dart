import 'dart:io';

import 'package:facebook_audience_network/ad/ad_interstitial.dart';
import 'package:facebook_audience_network/facebook_audience_network.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:korean_app/views/widgets/MyBanner.dart';

class AdsController{

  static bool _isInterstitialAdLoaded = false;
  static bool isBannerFail = true;

  AdsController(){
    _facebookAdsInit();
    loadInterstititalAds();
  }

  static _facebookAdsInit() {
    print("under facebook ads");
    FacebookAudienceNetwork.init(
        testingId: "37b1da9d-b48c-4103-a393-2e095e734bd6"
    );
  }
  static loadInterstititalAds(){
    //interstial "1106515326393799_1106516583060340"
    FacebookInterstitialAd.loadInterstitialAd(
        placementId: "1106515326393799_1106516583060340",
        listener: (result, value){
          print("Result was $result --> $value");
          if(result == InterstitialAdResult.LOADED && _isInterstitialAdLoaded == false){
            print("Connecting to facebook was $result");
            FacebookInterstitialAd.showInterstitialAd(delay:1000);
            _isInterstitialAdLoaded = true;
          }else{
            print("Called already");
          }
          if(result == InterstitialAdResult.DISMISSED && value["invalidated"] == true) {
            //  _isInterstitialAdLoaded = false;
            //  _loadInterstititalAds();
          }
        }
    );
  }
  static Widget loadBanner(){
    return load_FB_Banner();
  }
  static Future<Widget> loadBanner_future() async {

    var result;

    try {
      result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        return load_FB_Banner();
//        return Future.value(load_FB_Banner());
      }
    }on SocketException catch(_){
      print('not connected');
      return substituteBanner();
  //    return Future.value(substituteBanner());
    }
  }
  static Widget load_FB_Banner(){
    print("under LoadBanner()");
    return FacebookBannerAd(
      placementId: "1106515326393799_1107281416317190",
      bannerSize: BannerSize.STANDARD,
      listener: (result, value) {
        switch (result) {
          case BannerAdResult.ERROR:
            print("Error: $value");
            break;
          case BannerAdResult.LOADED:
            print("Loaded: $value");
            break;
          case BannerAdResult.CLICKED:
            print("Clicked: $value");
            break;
          case BannerAdResult.LOGGING_IMPRESSION:
            print("Logging Impression: $value");
            break;
        }
      },
    );
  }

  static Widget load_FB_NativeAd(){
    return FacebookNativeAd(
      placementId:"1106515326393799_1107281416317190",
      adType: NativeAdType.NATIVE_AD,
      width: double.infinity,
      height: 300,
      backgroundColor: Colors.blue,
      titleColor: Colors.white,
      descriptionColor: Colors.white,
      buttonColor: Colors.deepPurple,
      buttonTitleColor: Colors.white,
      buttonBorderColor: Colors.white,
      listener: (result, value) {
        print("Native Ad: $result --> $value");
      },
    );
  }
  static Widget load_FB_NativeBanner(){
    return FacebookNativeAd(
      placementId:"1106515326393799_1107281416317190",
      adType: NativeAdType.NATIVE_BANNER_AD,
      bannerAdSize: NativeBannerAdSize.HEIGHT_50,
      width: double.infinity,
      backgroundColor: Colors.blue,
      titleColor: Colors.white,
      descriptionColor: Colors.white,
      buttonColor: Colors.deepPurple,
      buttonTitleColor: Colors.white,
      buttonBorderColor: Colors.white,
      listener: (result, value) {
        print("Native Ad: $result --> $value");
        if(result == NativeAdResult.ERROR){

        }
      },
    );
  }

  static Widget substituteBanner(){
    print("substituteBanner()");
    return MyBanner();
  }


}