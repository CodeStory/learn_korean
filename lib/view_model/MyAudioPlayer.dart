import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';

class MyAudioPlayer{
  static AudioPlayer ap = AudioPlayer();
  static bool inUse = false;

  static playButton(int index,dynamic data) async{

    data.isPlaying = true;

    if(inUse){
      _inUse(data);
    }else{
      _notInUse(data);
    }
  }

  static _inUse(dynamic selectedData){
    stopMedia();
    if(selectedData.isPlaying) {
      inUse = true;
      AudioCache audioCache = AudioCache(prefix: 'sounds/', fixedPlayer: ap);
      //audioCache.play('five-2.mp3');
      print(selectedData.soundPath);
      audioCache.play(selectedData.soundPath);

      ap.onPlayerCompletion.listen((event) {
        ap.release();
        audioCache.clearCache();
        selectedData.isPlaying = false;
        inUse = false;
      });
    }
  }
  static _notInUse(dynamic selectedData){
    if(selectedData.isPlaying){
      inUse = true;

      AudioCache audioCache = AudioCache(prefix:'sounds/', fixedPlayer: ap);
      //audioCache.play('five.mp3');
      audioCache.play(selectedData.soundPath);

      ap.onPlayerCompletion.listen((event) {
        ap.release();
        audioCache.clearCache();
        selectedData.isPlaying = false;
        inUse = false;
      });
    }
  }
  static stopMedia(){
    ap.stop();
    ap.release();
  }

}