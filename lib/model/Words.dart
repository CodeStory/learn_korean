
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Words{
  LoveWords loveWords;
  BusinessWords businessWords;
  TravelWords travelWords;

  SupermarketWords supermarketWords;
  RestaurantWords restaurantWords;
  ShoppingWords shoppingWords;

  FamilyWords familyWords;
  FriendWords friendWords;
  AnimalWords animalWords;

  EmergencyWords emergencyWords;
  HospitalWords hospitalWords;
  RandomWords randomWords;

  Words({
    this.loveWords, this.businessWords, this.travelWords,
    this.supermarketWords, this.restaurantWords, this.shoppingWords,
    this.familyWords, this.friendWords, this.animalWords,
    this.randomWords, this.emergencyWords, this.hospitalWords,
  });

  factory Words.fromJson(Map<String, dynamic> parsedJson){
    return Words(
      loveWords: LoveWords.fromJson(parsedJson['love_words']),
      businessWords:BusinessWords.fromJson(parsedJson['business_words']),
      travelWords: TravelWords.fromJson(parsedJson["travel_words"]),

      supermarketWords: SupermarketWords.fromJson(parsedJson['supermarket_words']),
      restaurantWords: RestaurantWords.fromJson(parsedJson['restaurant_words']),
      shoppingWords: ShoppingWords.fromJson(parsedJson['shopping_words']),

      familyWords:FamilyWords.fromJson(parsedJson['family_words']),
      friendWords:FriendWords.fromJson(parsedJson['friend_words']),
      animalWords:AnimalWords.fromJson(parsedJson['animal_words']),

      randomWords: RandomWords.fromJson(parsedJson['random_words']),
      emergencyWords: EmergencyWords.fromJson(parsedJson['emergency_words']),
      hospitalWords: HospitalWords.fromJson(parsedJson['hospital_words']),
    );
  }
}

class LoveWords{
  final List<MaleWords> maleWords;
  final List<FemaleWords> femaleWords;
  final String image;
  final String title;

//  MaleWords maleWords;
 // FemaleWords femaleWords;
  LoveWords({this.maleWords, this.femaleWords, this.image, this.title});

//factory LoveWords.fromJson(Map<String,dynamic> parsedJson){
  factory LoveWords.fromJson(Map<String,dynamic> parsedJson){
    return LoveWords(
        maleWords:List<MaleWords>.from(parsedJson["male"].map((i)=>MaleWords.fromJson(i))),
        femaleWords:List<FemaleWords>.from(parsedJson["female"].map((i)=>FemaleWords.fromJson(i))),// _femaleWords,
        image:parsedJson["image"],
        title:parsedJson["title"],
      );
  }
}

class MaleWords{
  /*
  String mKoreanWord;-
  String mPronoun;
  String mEngDef;
  File mSoundFile;
  IconData _iconPlayButton;
  var _mIsSelected = false;

  Words(String koreanW, pronoun, engDef,File soundFile){
    this.mKoreanWord = koreanW;
    this.mPronoun = pronoun;
    this.mEngDef = engDef;
    this.mSoundFile = soundFile;
    _iconPlayButton = Icons.play_arrow;
  }
  getKoreanWords(){return mKoreanWord;}
  getPronoun(){return mPronoun;}
  getSoundPath(){return mSoundFile;}
  getPlayButtonIcon(){return _iconPlayButton;}
  getIsSelected(){return _mIsSelected;}

    setIsSelected(bool isSelected){
    _mIsSelected = isSelected;
  }


 */
  IconData _iconPlayButton;
  setIconPlayButton(IconData newIcon){
    _iconPlayButton = newIcon;
  }
  final String id;
  final String koreanWord;
  final String pronoun;
  final String engDef;
  final String soundPath;
  //final File soundFile;
  IconData iconPlayButton = Icons.play_arrow;
  var isSelected = false;
  bool isPlaying = false;

  MaleWords({this.id, this.koreanWord,this.pronoun, this.engDef, this.soundPath});
  factory MaleWords.fromJson(dynamic parsedJson){
    return MaleWords(
        id:parsedJson['id'],
        koreanWord: parsedJson['korean'],
        pronoun: parsedJson['pronoun'],
        engDef: parsedJson['english'],
        //soundFile: new File(parsedJson['soundPath'])
        soundPath: parsedJson['soundPath']
    );
  }
}
class FemaleWords{
  /*
  String mKoreanWord;
  String mPronoun;
  String mEngDef;
  File mSoundFile;
  IconData _iconPlayButton;
  var _mIsSelected = false;

  Words(String koreanW, pronoun, engDef,File soundFile){
    this.mKoreanWord = koreanW;
    this.mPronoun = pronoun;
    this.mEngDef = engDef;
    this.mSoundFile = soundFile;
    _iconPlayButton = Icons.play_arrow;
  }
  getKoreanWords(){return mKoreanWord;}
  getPronoun(){return mPronoun;}
  getSoundPath(){return mSoundFile;}
  getPlayButtonIcon(){return _iconPlayButton;}
  getIsSelected(){return _mIsSelected;}

    setIsSelected(bool isSelected){
    _mIsSelected = isSelected;
  }


 */
  IconData _iconPlayButton;
  setIconPlayButton(IconData newIcon){
    _iconPlayButton = newIcon;
  }
  final String id;
  final String koreanWord;
  final String pronoun;
  final String engDef;
  //final File soundFile;
  final String soundPath;
  IconData iconPlayButton = Icons.play_arrow;
  var isSelected = false;
  bool isPlaying = false;

  FemaleWords({this.id, this.koreanWord,this.pronoun, this.engDef, this.soundPath});
  factory FemaleWords.fromJson(dynamic parsedJson){
    return FemaleWords(
        id:parsedJson['id'],
        koreanWord: parsedJson['korean'],
        pronoun: parsedJson['pronoun'],
        engDef: parsedJson['english'],
        soundPath: parsedJson['soundPath']
        //soundFile: new File(parsedJson['soundPath'])
    );
  }
}

class BusinessWords{
  final List<MaleWords> maleWords;
  final List<FemaleWords> femaleWords;
  //final List<ToSuperior> toSuperior;
  //final List<ToSubordinate> toSubordinate;
  final String image;
  final String title;
/*
  BusinessWords({this.toSuperior, this.toSubordinate, this.image,this.title});
*/
  BusinessWords({this.maleWords, this.femaleWords, this.image,this.title});
  factory BusinessWords.fromJson(Map<String,dynamic> parsedJson){
    return BusinessWords(
      maleWords:List<MaleWords>.from(parsedJson["male"].map((i)=>MaleWords.fromJson(i))),
      femaleWords:List<FemaleWords>.from(parsedJson["female"].map((i)=>FemaleWords.fromJson(i))),
      image:parsedJson["image"],
      title:parsedJson["title"],
    );
  }
}
class ToSuperior {
  final String koreanWord;
  final String pronoun;
  final String engDef;
  final File soundFile;
  IconData iconPlayButton = Icons.play_arrow;
  var isSelected = false;
  bool isPlaying = false;

  ToSuperior({this.koreanWord,this.pronoun, this.engDef, this.soundFile});
  factory ToSuperior.fromJson(dynamic parsedJson){
    return ToSuperior(
        koreanWord: parsedJson['korean'],
        pronoun: parsedJson['pronoun'],
        engDef: parsedJson['english'],
        soundFile: new File(parsedJson['soundPath'])
    );
  }

}
class ToSubordinate{
  final String koreanWord;
  final String pronoun;
  final String engDef;
  final File soundFile;
  IconData iconPlayButton = Icons.play_arrow;
  var isSelected = false;
  bool isPlaying = false;

  ToSubordinate({this.koreanWord,this.pronoun, this.engDef, this.soundFile});
  factory ToSubordinate.fromJson(dynamic parsedJson){
    return ToSubordinate(
        koreanWord: parsedJson['korean'],
        pronoun: parsedJson['pronoun'],
        engDef: parsedJson['english'],
        soundFile: new File(parsedJson['soundPath'])
    );
  }
}

class TravelWords{
  //final List<Direction> direction;
  //final List<Shopping> shopping;
  final List<MaleWords> maleWords;
  final List<FemaleWords> femaleWords;
  final String image;
  final String title;

  TravelWords({this.maleWords, this.femaleWords, this.image, this.title});

  factory TravelWords.fromJson(Map<String,dynamic> parsedJson){
    return TravelWords(
        maleWords:List<MaleWords>.from(parsedJson["male"].map((i)=>MaleWords.fromJson(i))),
        femaleWords:List<FemaleWords>.from(parsedJson["female"].map((i)=>FemaleWords.fromJson(i))),
        image:parsedJson["image"],
        title:parsedJson["title"],
    );
  }
}
class Direction {
  final String koreanWord;
  final String pronoun;
  final String engDef;
  final File soundFile;
  IconData iconPlayButton = Icons.play_arrow;
  var isSelected = false;
  bool isPlaying = false;

  Direction({this.koreanWord,this.pronoun, this.engDef, this.soundFile});
  factory Direction.fromJson(dynamic parsedJson){
    return Direction(
        koreanWord: parsedJson['korean'],
        pronoun: parsedJson['pronoun'],
        engDef: parsedJson['english'],
        soundFile: new File(parsedJson['soundPath'])
    );
  }
}
class Shopping{
  final String koreanWord;
  final String pronoun;
  final String engDef;
  final File soundFile;
  IconData iconPlayButton = Icons.play_arrow;
  var isSelected = false;
  bool isPlaying = false;

  Shopping({this.koreanWord,this.pronoun, this.engDef, this.soundFile});
  factory Shopping.fromJson(dynamic parsedJson){
    return Shopping(
        koreanWord: parsedJson['korean'],
        pronoun: parsedJson['pronoun'],
        engDef: parsedJson['english'],
        soundFile: new File(parsedJson['soundPath'])
    );
  }
}

class SupermarketWords{
  final List<MaleWords> maleWords;
  final List<FemaleWords> femaleWords;
  final String image;
  final String title;

//  MaleWords maleWords;
  // FemaleWords femaleWords;
  SupermarketWords({this.maleWords, this.femaleWords, this.image, this.title});

//factory LoveWords.fromJson(Map<String,dynamic> parsedJson){
  factory SupermarketWords.fromJson(Map<String,dynamic> parsedJson){
    return SupermarketWords(
      maleWords:List<MaleWords>.from(parsedJson["male"].map((i)=>MaleWords.fromJson(i))),
      femaleWords:List<FemaleWords>.from(parsedJson["female"].map((i)=>FemaleWords.fromJson(i))),// _femaleWords,
      image:parsedJson["image"],
      title:parsedJson["title"],
    );
  }
}
class RestaurantWords{
  final List<MaleWords> maleWords;
  final List<FemaleWords> femaleWords;
  final String image;
  final String title;

//  MaleWords maleWords;
  // FemaleWords femaleWords;
  RestaurantWords({this.maleWords, this.femaleWords, this.image, this.title});

//factory LoveWords.fromJson(Map<String,dynamic> parsedJson){
  factory RestaurantWords.fromJson(Map<String,dynamic> parsedJson){
    return RestaurantWords(
      maleWords:List<MaleWords>.from(parsedJson["male"].map((i)=>MaleWords.fromJson(i))),
      femaleWords:List<FemaleWords>.from(parsedJson["female"].map((i)=>FemaleWords.fromJson(i))),// _femaleWords,
      image:parsedJson["image"],
      title:parsedJson["title"],
    );
  }
}
class ShoppingWords{
  final List<MaleWords> maleWords;
  final List<FemaleWords> femaleWords;
  final String image;
  final String title;

//  MaleWords maleWords;
  // FemaleWords femaleWords;
  ShoppingWords({this.maleWords, this.femaleWords, this.image, this.title});

//factory LoveWords.fromJson(Map<String,dynamic> parsedJson){
  factory ShoppingWords.fromJson(Map<String,dynamic> parsedJson){
    return ShoppingWords(
      maleWords:List<MaleWords>.from(parsedJson["male"].map((i)=>MaleWords.fromJson(i))),
      femaleWords:List<FemaleWords>.from(parsedJson["female"].map((i)=>FemaleWords.fromJson(i))),// _femaleWords,
      image:parsedJson["image"],
      title:parsedJson["title"],
    );
  }
}

FamilyWords familyWords;
FriendWords friendWords;
AnimalWords anmialWords;

class FamilyWords{
  final List<MaleWords> maleWords;
  final List<FemaleWords> femaleWords;
  final String image;
  final String title;

//  MaleWords maleWords;
  // FemaleWords femaleWords;
  FamilyWords({this.maleWords, this.femaleWords, this.image, this.title});

//factory LoveWords.fromJson(Map<String,dynamic> parsedJson){
  factory FamilyWords.fromJson(Map<String,dynamic> parsedJson){
    return FamilyWords(
      maleWords:List<MaleWords>.from(parsedJson["male"].map((i)=>MaleWords.fromJson(i))),
      femaleWords:List<FemaleWords>.from(parsedJson["female"].map((i)=>FemaleWords.fromJson(i))),// _femaleWords,
      image:parsedJson["image"],
      title:parsedJson["title"],
    );
  }
}
class FriendWords{
  final List<MaleWords> maleWords;
  final List<FemaleWords> femaleWords;
  final String image;
  final String title;

//  MaleWords maleWords;
  // FemaleWords femaleWords;
  FriendWords({this.maleWords, this.femaleWords, this.image, this.title});

//factory LoveWords.fromJson(Map<String,dynamic> parsedJson){
  factory FriendWords.fromJson(Map<String,dynamic> parsedJson){
    return FriendWords(
      maleWords:List<MaleWords>.from(parsedJson["male"].map((i)=>MaleWords.fromJson(i))),
      femaleWords:List<FemaleWords>.from(parsedJson["female"].map((i)=>FemaleWords.fromJson(i))),// _femaleWords,
      image:parsedJson["image"],
      title:parsedJson["title"],
    );
  }
}
class AnimalWords{
  final List<MaleWords> maleWords;
  final List<FemaleWords> femaleWords;
  final String image;
  final String title;

//  MaleWords maleWords;
  // FemaleWords femaleWords;
  AnimalWords({this.maleWords, this.femaleWords, this.image, this.title});

//factory LoveWords.fromJson(Map<String,dynamic> parsedJson){
  factory AnimalWords.fromJson(Map<String,dynamic> parsedJson){
    return AnimalWords(
      maleWords:List<MaleWords>.from(parsedJson["male"].map((i)=>MaleWords.fromJson(i))),
      femaleWords:List<FemaleWords>.from(parsedJson["female"].map((i)=>FemaleWords.fromJson(i))),// _femaleWords,
      image:parsedJson["image"],
      title:parsedJson["title"],
    );
  }
}

class EmergencyWords{
  final List<MaleWords> maleWords;
  final List<FemaleWords> femaleWords;
  final String image;
  final String title;

//  MaleWords maleWords;
  // FemaleWords femaleWords;
  EmergencyWords({this.maleWords, this.femaleWords, this.image, this.title});

//factory LoveWords.fromJson(Map<String,dynamic> parsedJson){
  factory EmergencyWords.fromJson(Map<String,dynamic> parsedJson){
    return EmergencyWords(
      maleWords:List<MaleWords>.from(parsedJson["male"].map((i)=>MaleWords.fromJson(i))),
      femaleWords:List<FemaleWords>.from(parsedJson["female"].map((i)=>FemaleWords.fromJson(i))),// _femaleWords,
      image:parsedJson["image"],
      title:parsedJson["title"],
    );
  }
}
class HospitalWords{
  final List<MaleWords> maleWords;
  final List<FemaleWords> femaleWords;
  final String image;
  final String title;

//  MaleWords maleWords;
  // FemaleWords femaleWords;
  HospitalWords({this.maleWords, this.femaleWords, this.image, this.title});

//factory LoveWords.fromJson(Map<String,dynamic> parsedJson){
  factory HospitalWords.fromJson(Map<String,dynamic> parsedJson){
    return HospitalWords(
      maleWords:List<MaleWords>.from(parsedJson["male"].map((i)=>MaleWords.fromJson(i))),
      femaleWords:List<FemaleWords>.from(parsedJson["female"].map((i)=>FemaleWords.fromJson(i))),// _femaleWords,
      image:parsedJson["image"],
      title:parsedJson["title"],
    );
  }
}
class RandomWords{
  final List<MaleWords> maleWords;
  final List<FemaleWords> femaleWords;
  final String image;
  final String title;

//  MaleWords maleWords;
  // FemaleWords femaleWords;
  RandomWords({this.maleWords, this.femaleWords, this.image, this.title});

//factory LoveWords.fromJson(Map<String,dynamic> parsedJson){
  factory RandomWords.fromJson(Map<String,dynamic> parsedJson){
    return RandomWords(
      maleWords:List<MaleWords>.from(parsedJson["male"].map((i)=>MaleWords.fromJson(i))),
      femaleWords:List<FemaleWords>.from(parsedJson["female"].map((i)=>FemaleWords.fromJson(i))),// _femaleWords,
      image:parsedJson["image"],
      title:parsedJson["title"],
    );
  }
}
