import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:korean_app/model/Words.dart';

class Services{
  Future fetchWords() async{
    /*If from server
    final url = "http://..."
    final response = await http.get(url);
    if(response.status Code == 200){

    }
     */

    //load local json file.
    String jsonWord = await _loadWordAsset();
    //final Map<String, dynamic> jsonResponse = json.decode(jsonWord);
    final dynamic jsonResponse = json.decode(jsonWord);

    Words words = Words.fromJson(jsonResponse);
    //LoveWords words = LoveWords.fromJson(jsonResponse['love_words']);
    //List<MaleWords> words = (jsonResponse['loveWords']['male'] as List).map((i)=> MaleWords.fromJson(i)).toList();

    if(jsonResponse != null){
      return words;
      //return Words.fromJson(jsonResponse); //jsonResponse.map((i) => Words.fromJson(i));  // Words.fromJson(jsonResponse);
    }
    else {throw Exception("Unable to perform request: Failed to read JSON");}


  }
  Future<String>_loadWordAsset() async {
    return await rootBundle.loadString('assets/data_repo/word_data.json');
  }

}